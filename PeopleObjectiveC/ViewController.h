//
//  ViewController.h
//  PeopleObjectiveC
//
//  Created by ios on 06.03.17.
//  Copyright © 2017 NSObject. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
- (IBAction)ageClick:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerHeight;
- (IBAction)doneDate:(id)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerAge;


@end

