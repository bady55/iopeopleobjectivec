//
//  PeopleModel.h
//  PeopleObjectiveC
//
//  Created by ios on 06.03.17.
//  Copyright © 2017 NSObject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PeopleModel : NSObject

@property (copy) NSString *model;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, assign) NSInteger age;
@property (nonatomic, assign) BOOL sex;

@end
