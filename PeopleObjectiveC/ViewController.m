//
//  ViewController.m
//  PeopleObjectiveC
//
//  Created by ios on 06.03.17.
//  Copyright © 2017 NSObject. All rights reserved.
//

#import "ViewController.h"
#import "PeopleModel.h"

@interface ViewController () <UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *firstNameOutlet;
@property (weak, nonatomic) IBOutlet UITextField *lastNameOutlet;
@property (weak, nonatomic) IBOutlet UITextField *ageOutlet;
@property (weak, nonatomic) IBOutlet UITextField *countryOutlet;
@property (weak, nonatomic) IBOutlet UISwitch *sexOutlet;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.datePickerHeight.constant = 0;
    self.firstNameOutlet.delegate = self;
    self.lastNameOutlet.delegate = self;
    self.countryOutlet.delegate = self;
    self.ageOutlet.delegate = self;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showPeopleAction:(id)sender {
    
    PeopleModel *model = [[PeopleModel alloc] init];
    model.firstName = self.firstNameOutlet.text;
    model.lastName = self.lastNameOutlet.text;
    model.age = [self.ageOutlet.text integerValue];
    model.sex = self.sexOutlet.isOn;
    model.country = self.countryOutlet.text;
    
    NSString *resultString = [NSString stringWithFormat:@"First Name: %@, \nLast name: %@, \nAge: %ld , \nIs male %@, \nCountry %@", model.firstName, model.lastName, (long)model.age, model.sex ? @"female" : @"male", model.country];
    
    
    UIAlertController *alertController = [UIAlertController
                                alertControllerWithTitle:@"Person is"
                                message: resultString
                                preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 0;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 1;
}

- (IBAction)ageClick:(id)sender {
    self.datePickerHeight.constant = 196;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _ageOutlet)
        return NO;
    else
        return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [_firstNameOutlet resignFirstResponder];
    [_lastNameOutlet resignFirstResponder];
    return YES;
}
- (IBAction)doneDate:(id)sender {
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:self.datePickerAge.date
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    self.ageOutlet.text = [NSString stringWithFormat:@"%ld", (long)age];
    
    self.datePickerHeight.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}
@end
